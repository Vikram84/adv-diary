from django.urls import path
from . import views

urlpatterns = [
    path('', views.homepage),
    path('signup', views.register),
    path('forgot_credentials', views.forgot_credentials),
    path('add_client', views.add_client),
]