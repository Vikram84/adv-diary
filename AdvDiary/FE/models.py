from django.db import models

# Create your models here.
class ClientModel(models.Model):
    cid = models.IntegerField(primary_key=True, auto_created=True)
    name = models.CharField(max_length=200)
    mobile = models.CharField(max_length=25)
    email = models.EmailField(max_length=75)
