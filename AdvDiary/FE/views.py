from django.shortcuts import render
from .forms import ClientForm

# Create your views here.
def homepage(request):
    context = { 'client_form' : ClientForm }
    return render(request, 'index.html', context=context)

def register(request):
    return render(request, 'signup.html')

def forgot_credentials(request):
    return render(request, 'forgot_credentials.html')

def add_client(request):
    try:
        frm = ClientForm(request.POST)
        if frm.is_valid():
            frm.save()
    except:
        return render(request, 'message.html', context={'msg': "Internal Error"})
    return render(request, "message.html", context={'msg': 'Added Client Successfully'})
    